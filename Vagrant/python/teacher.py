import fabtools
from fabric.api import *

class Teacher(object):
    
    def __init__(self, username=None, password=None, first_name=None, last_name=None):
        self.username = username
        self.password = password
        self.first_name = first_name
        self.last_name = last_name

        # Directories
        self.student_dir_home = '/home/{}/uczelnia'.format(username)


    def create_user(self):
        if not fabtools.group.exists('teachers'):
            fabtools.group.create('teachers')
        if not fabtools.user.exists(self.username):
            comment = '{} {}'.format(self.first_name, self.last_name)
            fabtools.user.create(self.username, password=self.password, comment=comment, group='teachers')
            sudo('passwd -e {}'.format(self.username))
        else:
            print 'User exists!'
            return False
        return True

    def grant_access_teacher_group(self):
        sudo('setfacl -Rm g:teachers:rwx /srv')
