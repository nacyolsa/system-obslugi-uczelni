
# Directory names
BASE_DIR = '/srv/uczelnia/roczniki'
SHARED_DIR = 'Katalog_wspolny'
NOTES = 'Uwagi'
REPORTS = 'Sprawozdania'

# ACL permissions
DEFAULT_ACL_BASE_DIR = ['u::---', 'g::---', 'o:---']
ACL_STUDENTS_BASE_DIR = 'g:students:r-x'

# Fabric config
HOSTS = ['localhost']
USER = 'administrator'
PASSWORD = '123'
