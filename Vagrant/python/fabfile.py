import os
import time
import fabtools
from fabric.api import *
from fabric.state import output

import settings
from student import Student
from teacher import Teacher
from password import random_string_generator
from gen_data import generate_students, generete_teachers, grant_access

env.hosts    = settings.HOSTS
env.user     = settings.USER
env.password = settings.PASSWORD
# Output control
# output.debug = False
# output.status = False
# output.aborts = False
# output.warnings = False
# output.running = False
# output.stdout = False
# output.stderr = False
# output.user = False

# Create BASE_DIR and set ACL for all ACL members on ---
@task
def dfacl():
    sudo('mkdir -p {}'.format(settings.BASE_DIR))
    Student().clear_acl_base_dir()
    Student().set_acl_base_dir()

# Create new user, directories
@task
def dodajstudenta(imie=None, nazwisko=None, rok=None, kierunek=None, specjalnosc=None,
             grupa=None, przedmiot=None, login=None, haslo=None):
    if not login:
        login = str(int(time.time())) + 's'
    if not haslo:
        haslo = random_string_generator()
    student = Student(first_name=imie, last_name=nazwisko, year=rok, field=kierunek, spec=specjalnosc,
                      group=grupa, subject=przedmiot, username=login, password=haslo)
    if student.create_user():
        student.create_students_dirs()
        student.grant_access()
        student.create_links()
        print 'Utworzono: login = {}  haslo = {}'.format(student.username, student.password)
    else:
        print 'Wystapil problem przy tworzeniu uzytkownika {} !'.format(login)

# Create new user (lecturer)
@task
def dodajprowadzacego(imie=None, nazwisko=None, login=None, haslo=None):
    if not login:
        login = str(int(time.time())) + 't'
    if not haslo:
        haslo = random_string_generator()
    teacher = Teacher(username=login, password=haslo, first_name=imie, last_name=nazwisko)
    if teacher.create_user():
        print 'Utworzono: login = {}  haslo = {}'.format(teacher.username, teacher.password)
    else:
        print 'Wystapil problem przy tworzeniu uzytkownika {} !'.format(login)

# Grant access for lecturer
@task
def nadajprawa(przedmiot, student, prowadzacy):
    Student(username=student, subject=przedmiot).grant_access_teacher(prowadzacy)

# Disable student account
@task
def wylaczstudenta(login):
    sudo('chage -E 0 {}'.format(login))
    path = '/home/{}/uczelnia/{}'.format(login, login)
    real_path = os.path.realpath(path)
    sudo('chattr -R -a {}'.format(real_path))
    sudo('chattr -R +iu {}'.format(real_path))
    user_home = '/home/{}'.format(login)
    sudo('chattr -R +iu {}'.format(user_home))

# Print student server path
@task
def sciezkastudenta(login):
    print Student(username=login).get_real_path()


@task
def rocznikstudenta(login):
    print Student(username=login).get_year()


@task
def kierunekstudenta(login):
    print Student(username=login).get_field()


@task
def specjalnoscstudenta(login):
    print Student(username=login).get_spec()


@task
def grupastudenta(login):
    print Student(username=login).get_group()


@task
def przedmiotystudenta(login):
    print Student(username=login).get_subjects()

# Backup for student account
@task
def kopiazapasowa(login):
    path = '/backup/{}'.format(login)
    sudo('mkdir -p {}'.format(path))
    real_path = Student(username=login).get_real_path()
    # sudo('chattr -R -ia {}'.format(real_path))
    rsync = 'rsync -arbudlpAXgtz {} {}'.format(real_path, path)
    home_path = '/home/{}'.format(login)
    rsync_home = 'rsync -arbudlpAXgtz {} {}'.format(home_path, path)
    sudo(rsync)
    sudo(rsync_home)

# Generate students accounts for given amount and save user:pass to admin home dir
@task
def genstd(amount):
    generate_students(amount)

# Generate teachers accounts for given amount and save user:pass to admin home dir
@task
def genteach(amount):
    generete_teachers(amount)


@task
def grantaccgen():
    grant_access()

@task
def test():
    sudo('echo "Hello!"')



