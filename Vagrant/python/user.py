import os

class User(object):
    def __init__(self, username=None, password=None, first_name=None, last_name=None):
        self.username = username
        self.password = password
        self.first_name = first_name
        self.last_name = last_name

    def get_real_path(self):
        link = '/home/{}/uczelnia/{}'.format(self.username, self.username)
        real_path = os.path.realpath(link)
        return real_path
