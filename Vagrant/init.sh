
su administrator << 'EOF'
cd /vagrant/python
export LC_ALL=C
pip install -r requirements.txt
cp -r /vagrant/python ~/python
sudo chmod -R o-rwx /home/administrator/
sudo cp /vagrant/sou.sh /usr/local/sbin/
echo "export FABFILE=/home/administrator/python/fabfile.py" >> ~/.bashrc

echo "alias sou='sudo -E sou.sh'" >> ~/.bashrc
EOF
