
sudo useradd -m administrator
sudo usermod -aG sudo administrator
sudo passwd administrator

su administrator << 'EOF'
sudo apt-get update
sudo apt-get install -y apt-transport-https lsb-release systemd systemd-sysv ca-certificates gnupg2 ntpdate tree

sudo apt-get update
sudo apt-get install -y build-essential python python-dev python-setuptools python-pip fabric

#sudo rm /etc/ssh/sshd_config
#sudo apt-get purge -y openssh-server
#sudo apt-get install -y openssh-server

export LC_ALL=C
pip install -r python/requirements.txt
cp -r python ~/python
sudo chmod -R o-rwx /home/administrator/
sudo cp sou.sh /usr/local/sbin/
echo "export FABFILE=/home/administrator/python/fabfile.py" >> ~/.bashrc

echo "alias sou='sudo -E sou.sh'" >> ~/.bashrc
EOF
