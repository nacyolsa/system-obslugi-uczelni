import os
import csv
import time
import fabtools
from fabric.api import *

from user import *
import settings
from password import random_string_generator

class Student(User):

    def __init__(self, year=None, field=None, spec=None, group=None, subject=None,
                 username=None, password=None, first_name=None, last_name=None):
        self.year = year
        self.field = field
        self.spec = spec
        self.group = group
        self.subject = subject
        self.username = username
        self.password = password
        self.first_name = first_name
        self.last_name = last_name

        # Directories
        self.student_dir_field   = '{}/{}/{}/{}'.format(settings.BASE_DIR, self.year, self.field, settings.SHARED_DIR)
        self.student_dir_spec    = '{}/{}/{}/{}/{}'.format(settings.BASE_DIR, self.year, self.field, self.spec, settings.SHARED_DIR)
        self.student_dir_group   = '{}/{}/{}/{}/{}/{}'.format(settings.BASE_DIR, self.year, self.field, self.spec, self.group, settings.SHARED_DIR)
        self.student_dir_subject = '{}/{}/{}/{}/{}/{}/{}/{}'.format(settings.BASE_DIR, self.year, self.field, self.spec,
                                                                    self.group, self.username, self.subject, settings.SHARED_DIR)
        self.student_dir_report  = '{}/{}/{}/{}/{}/{}/{}/{}'.format(settings.BASE_DIR, self.year, self.field, self.spec,
                                                                    self.group, self.username, self.subject, settings.REPORTS)
        self.student_dir_notes   = '{}/{}/{}/{}/{}/{}/{}/{}'.format(settings.BASE_DIR, self.year, self.field, self.spec,
                                                                    self.group, self.username, self.subject, settings.NOTES)
        self.student_dir         = '{}/{}/{}/{}/{}/{}'.format(settings.BASE_DIR, self.year, self.field, self.spec,
                                                              self.group, self.username)
        self.student_dir_home    = '/home/{}/uczelnia'.format(username)


    def set_acl_base_dir(self):
        if not fabtools.group.exists('students'):
            fabtools.group.create('students')
        if not fabtools.group.exists('teachers'):
            fabtools.group.create('teachers')
        sudo('setfacl -m {} {}'.format(settings.ACL_STUDENTS_BASE_DIR, settings.BASE_DIR))
        sudo('setfacl -Rm g:students:r-x /srv')
        sudo('setfacl -Rm g:teachers:r-x /srv')
        sudo('setfacl -Rdm g:teachers:r-x /srv')
        for acl in settings.DEFAULT_ACL_BASE_DIR:
            sudo('setfacl -Rdm {} {}'.format(acl, settings.BASE_DIR))


    def clear_acl_base_dir(self):
        for acl in settings.DEFAULT_ACL_BASE_DIR:
            sudo('setfacl -Rm {} {}'.format(acl, settings.BASE_DIR))


    def create_user(self):
        if not fabtools.group.exists('students'):
            fabtools.group.create('students')
        if not fabtools.user.exists(self.username):
            comment = '{} {}'.format(self.first_name, self.last_name)
            fabtools.user.create(self.username, password=self.password, comment=comment, group='students')
            sudo('passwd -e {}'.format(self.username))
        else:
            print 'User exists!'
            return False
        return True


    def create_students_dirs(self):
        dirs = [self.student_dir_field, self.student_dir_spec, self.student_dir_group, self.student_dir_subject, self.student_dir_report, self.student_dir_notes, self.student_dir_home]
        for path in dirs:
            if not os.path.exists(path):
                os.makedirs(path)
                sudo('setfacl -m g:students:--- /home/{}'.format(self.username))
                sudo('setfacl -m o:--- /home/{}'.format(self.username))
                print 'Created directory: {}'.format(path)
        sudo('setfacl -Rdm u:{}:rwx {}'.format(self.username, self.student_dir_notes))


    def grant_access(self):
        path = settings.BASE_DIR
        pf = [self.year, self.field, settings.SHARED_DIR]
        ps =[self.year, self.field, self.spec, settings.SHARED_DIR]
        pg = [self.year, self.field, self.spec, self.group, settings.SHARED_DIR]
        pfull = [self.year, self.field, self.spec, self.group, self.username, self.subject, settings.SHARED_DIR] 
        for part in pf:
            path += '/' + part
            sudo('setfacl -m u:{}:r-x {}'.format(self.username, path))
        path = settings.BASE_DIR
        for part in ps:
            path += '/' + part
            sudo('setfacl -m u:{}:r-x {}'.format(self.username, path))
        path = settings.BASE_DIR
        for part in pg:
            path += '/' + part
            sudo('setfacl -m u:{}:r-x {}'.format(self.username, path))
        path = settings.BASE_DIR
        for part in pfull:
            path += '/' + part
            sudo('setfacl -m u:{}:r-x {}'.format(self.username, path))


    def create_links(self):
        field_shared = '/home/{}/uczelnia/{}_{}'.format(self.username, self.field, settings.SHARED_DIR)
        sudo('setfacl -Rm u:{}:r-x {}'.format(self.username, self.student_dir_field))
        fabtools.files.symlink(self.student_dir_field, field_shared, use_sudo=True)

        spec_shared = '/home/{}/uczelnia/{}_{}'.format(self.username, self.spec, settings.SHARED_DIR)
        sudo('setfacl -Rm u:{}:r-x {}'.format(self.username, self.student_dir_spec))
        fabtools.files.symlink(self.student_dir_spec, spec_shared, use_sudo=True)

        group_shared = '/home/{}/uczelnia/{}_{}'.format(self.username, self.group, settings.SHARED_DIR)
        sudo('setfacl -Rm u:{}:r-x {}'.format(self.username, self.student_dir_group))
        fabtools.files.symlink(self.student_dir_group, group_shared, use_sudo=True)

        sudo('setfacl -Rm u:{}:rwx {}'.format(self.username, self.student_dir))
        sudo('chattr -R +a {}'.format(self.student_dir))
        user_shared = '/home/{}/uczelnia/{}'.format(self.username, self.username)
        fabtools.files.symlink(self.student_dir, user_shared, use_sudo=True)


    def grant_access_teacher(self, teacher):
        link = '/home/{}/uczelnia/{}'.format(self.username, self.username)
        real_path = os.path.realpath(link)
        # Teacher account needs to be active.
        # if not fabtools.user.exists(teacher):
        #     print 'Lecturer {} does not exist'.format(teacher)
        #     return False
        # Temporary disable -a attribute from $link
        sudo('chattr -R -a {}'.format(real_path))

        sudo('setfacl -Rm u:{}:rwx {}'.format(self.username, real_path))
        sudo('setfacl -Rm u:{}:rwx {}'.format(teacher, real_path))
        link = '/home/{}/uczelnia/{}/{}'.format(self.username, self.username, self.subject)
        real_path = os.path.realpath(link)
        teacher_link = '/home/{}/{}_{}'.format(teacher, self.username, self.subject)
        fabtools.files.symlink(real_path, teacher_link, use_sudo=True)
        sudo('chattr -R +a {}'.format(real_path))


    def get_year(self):
        full_path = self.get_real_path()
        parts = full_path.split('/')
        return parts[4]


    def get_field(self):
        full_path = self.get_real_path()
        parts = full_path.split('/')
        return parts[5]


    def get_spec(self):
        full_path = self.get_real_path()
        parts = full_path.split('/')
        return parts[6]


    def get_group(self):
        full_path = self.get_real_path()
        parts = full_path.split('/')
        return parts[7]

    def get_subjects(self):
        full_path = self.get_real_path()
        return os.listdir(full_path)

