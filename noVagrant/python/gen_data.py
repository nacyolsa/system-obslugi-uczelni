import csv
import time
import random
from fabric.api import *

import settings
from student import Student
from teacher import Teacher
from password import random_string_generator


def generate_students(amount):
    years = ['2005', '2017', '2016', '2015', '2014']
    fields = ['informatyka', 'fizyka', 'polski', 'biologia', 'chemia']
    specs = ['ddd', 'tdd', 'bdd', 'asd', 'bla']
    groups = ['105cl', '104cl', '103cl', '102cl', '101cl']
    subjects = ['grafika', 'programowanie', 'systemy', 'angelski', 'bazy']
    path = '/home/{}/genstudents.csv'.format(settings.USER)
    usersFile = open(path, 'a')
    for x in range(0, int(amount)):
        username = str(int(time.time())) + 's'
        password = random_string_generator()
        year = random.choice(years)
        field = random.choice(fields)
        spec = random.choice(specs)
        group = random.choice(groups)
        subject = random.choice(subjects)
        student = Student(year=year, field=field, spec=spec,
                          group=group, subject=subject, username=username, password=password)
        if student.create_user():
            student.create_students_dirs()
            student.grant_access()
            student.create_links()
            print 'Created: username = {}  password = {}'.format(student.username, student.password)
            usersFile.write('{};{}\n'.format(username, password))
        else:
            print 'User cannot be created!'

    usersFile.close()
    print path


def generete_teachers(amount):
    path = '/home/{}/genteachers.csv'.format(settings.USER)
    usersFile = open(path, 'a')
    for x in range(0, int(amount)):
        username = str(int(time.time())) + 't'
        password = random_string_generator()
        teacher = Teacher(username=username, password=password)
        if teacher.create_user():
            print 'Created: username = {}  password = {}'.format(teacher.username, teacher.password)
            usersFile.write('{};{}\n'.format(username, password))
        else:
            print 'User cannot be created!'
        time.sleep(1)

    usersFile.close()
    print path


def grant_access():
    path_teachers = '/home/{}/genteachers.csv'.format(settings.USER)
    path_students = '/home/{}/genstudents.csv'.format(settings.USER)
    teachers = []
    students = []
    with open(path_teachers, 'rb') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=';')
        for row in readCSV:
            teachers.append(row[0])

    with open(path_students, 'rb') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=';')
        for row in readCSV:
            students.append(row[0])

    for student in students:
        subjects = Student(username=student).get_subjects()
        for sub in subjects:
            st = Student(username=student, subject=sub)
            st.grant_access_teacher(random.choice(teachers))
            subject_path = '{}/{}/Sprawozdania/spraw1'.format(st.get_real_path(), sub)
            sudo('echo "sprawko" > {}'.format(subject_path))

